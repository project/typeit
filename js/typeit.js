/*
* @file
*/

  (function ($) {
    'use strict';
    Drupal.behaviors.typeit = {
      attach: function (context, settings) {

        jQuery('#typeit-print-data').empty();
        var arr = [];
        $('#typeit-data').css('display', 'none');
        $('#typeit-data p').each(function (i, e) {
          arr.push($(e).text());
        });

        var typing_speed;
        var delete_speed;
        var cursor_speed;
        var loop_information;
        var breakline_information;
        var cursor_information;
        var loop_value;
        var breakline_data;
        var cursor_data;

        // To check wheter all the variable is set or not.
        if (settings.typeit != null) {

          // Value for textfield.
          typing_speed = parseInt(settings.typeit.time_intervel_typit);
          delete_speed = parseInt(settings.typeit.delete_speed_data);
          cursor_speed = parseInt(settings.typeit.cursor_sped_data_blink);

          // Value for radios.
          loop_information = parseInt(settings.typeit.loop_data_for_typit);
          breakline_information = parseInt(settings.typeit.break_lines_data);
          cursor_information = parseInt(settings.typeit.cursor_data_blink);

          // Conditions for boolean values.
          loop_value = (loop_information === 1) ? loop_value = true : loop_value = false;
          breakline_data = (breakline_information === 1) ? breakline_data = true : breakline_data = false;
          cursor_data = (cursor_information === 1) ? cursor_data = true : cursor_data = false;
        }
        else {
          typing_speed = 100;
          delete_speed = 100;
          cursor_speed = 1000;
          loop_value = false;
          breakline_data = true;
          cursor_data = true;
        }
        // Main typeit js values.
        $('#typeit-print-data').typeIt({
          strings: arr,
          speed: typing_speed,
          loop: loop_value,
          breakLines: breakline_data,
          deleteSpeed: delete_speed,
          cursor: cursor_data,
          cursorSpeed: cursor_speed,
          html: false
        });
      }
    };
  })(jQuery);
