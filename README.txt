All Credit goes to Alex macarther. He is the creator of
typeit js.

Here is link of the author

http://macarthur.me/typeit 



Follow https://www.drupal.org/u/akhilsoni1992

INTRODUCTION
------------

This module displays typeit affected text using block.
You have to just simply enter the text from the module
configuration page . Javascript values also can be modified
using configuration page of this module.


INSTALLATION
-------------

Generally, download module as you download any other
module and past it into you contributed module directory.

Enable module and config it from user-interface tab from
the configuration from your site.

Add your typeit data. You will find TypeIt named block
in your block section and place it on any region. You will
see your typeit effect for that block. 

REQUIREMENTS
------------

The basic file of jquery which is provided by drupal core is
the only requirement. But it is in drupal core. So no any
other external requirements.


CONFIGURATION
-------------

After enabling this module you have to configure setting 1st. 

 * To configure this module go to the following direction 
 
Admin >> configuration >> User Interface >> Typeit Settings

After done with configuring module you have to place typeit block
on any region.

Note :- CSS for the typeit text is provided by the user.
I have only provided 
CSS using normal point of view.
